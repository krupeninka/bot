# Скрипт для запуска ботов
#
# Пример запуска:
#     python bot.py --id 1
#
# Параметры:
#     id - ID бота, которого нужно запустить
#     debug - Ржим дебага

import argparse
import time
import traceback

from datetime import datetime
from importlib import __import__, reload

from app.drivers.vk_driver import VkDriver
from app.models.handler import Handler
from app.models.handler_task_log import HandlerTaskLog
from app.models.bot import Bot


class BotStarter(object):
    """ Класс отвечающий за запуск ботов, и выполнение заданий """


    def __init__(self, ids, debug):
        """ Инициализация обекта """
        self.debug = debug
        self.bots = Bot.select().where(Bot.id << ids.split(','), Bot.status == 'active')
        self.ids = [bot.id for bot in self.bots]

        self.drivers = {}
        self.modules = {}


    def run_drivers(self):
        """ Запуск драйверов для ботов """
        for bot in self.bots:
            driver = VkDriver()
            driver.login(login=bot.login, password=bot.password)

            self.drivers[bot.id] = driver


    def exec_next_handler(self):
        """ Выполнение следующей обработки """
        handler = self.next_handler()
        driver = self.drivers[handler.bot.id]
        start = datetime.now()
        lag = start - handler.next_run

        # подключаем модуль
        module_name = 'app.handlers.{}'.format(handler.module)
        module = __import__(module_name, fromlist=[handler.handler])
        if self.debug:
            reload(module)
        klass = getattr(module, handler.handler)

        # Инициализируем обработчик, и выполняем
        exhandler = klass(handler, driver)
        exhandler.execute()

        # Выводим в консоль информацию
        raw_bot = handler.bot.name.ljust(20)
        raw_handler = handler.raw_name().ljust(20)
        raw_stat_value = str(exhandler.stat_value).ljust(3)
        raw_lag = str(lag).ljust(15)
        raw_exec = str(datetime.now() - start).ljust(15)

        print('{}|{}|{}|lag: {}|exec: {}'.format(raw_bot, raw_handler, raw_stat_value, raw_lag, raw_exec))


    def quit_drivers(self):
        """ Остановка драйверов """
        for _, driver in self.drivers.items():
            driver.quit()


    def next_handler(self, wait=0):
        """ Поиск следующую обработку, для одного из активных ботов """
        while True:
            try:
                handler = Handler\
                        .select()\
                        .where( Handler.bot_id << self.ids,
                                Handler.status == 'enabled',
                                Handler.next_run < datetime.now())\
                        .order_by(Handler.next_step.asc())\
                        .get()
                break
            except Handler.DoesNotExist:
                wait += 1
                time.sleep(wait)

        return handler


if __name__ == "__main__":
    # Обработка параметров из консоли
    parser = argparse.ArgumentParser(description='Запуск ботов', add_help=False)
    parser.add_argument('--ids', help='Идентификатор запускаемого бота', required=True)
    parser.add_argument('--debug', help='Запуск бота в режиме отладки', default=False)
    args = parser.parse_args()

    # Запуск ботов
    starter = BotStarter(args.ids, args.debug)
    starter.run_drivers()
    while True:
        try:
            starter.exec_next_handler()
        except KeyboardInterrupt:
            print("Остановлено пользователем")
            break
        except Exception:
            print(traceback.format_exc())

    starter.quit_drivers()
