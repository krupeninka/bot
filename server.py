from app.flask import app

# Импортируем вьюхи
from app.views.main import *
from app.views.sessions import *
from app.views.bots import *
from app.views.handlers import *
from app.views.tasks import *
from app.views.mass_reports import *
from app.views.groups import *
from app.views.dobavs import *



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
