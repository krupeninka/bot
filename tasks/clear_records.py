import os
import sys

from datetime import datetime, timedelta

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from app.models.cache import Cache
from app.models.handler_task_log import HandlerTaskLog
from app.models.handler_task import HandlerTask

# Удаляем Кэш
Cache.delete().where(Cache.created_at < datetime.today() - timedelta(days = 7)).execute()

# Удаляем информацию о запуске обработчиков
HandlerTaskLog.delete().where(HandlerTaskLog.created_at < datetime.today() - timedelta(days = 7)).execute()
HandlerTask.delete().where(HandlerTask.created_at < datetime.today() - timedelta(days = 7)).execute()