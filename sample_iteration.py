import time
import random

class SimpleIterator:
    def __init__(self):
        self._counter = 0
        self._cache = []
        self._new_pages = []
        self._find_page_counter = 0

    def _next_page(self):
        """ Получение следующей страницы """

        # Если список страниц пустой, обновляем его из поиска
        if not self._new_pages:
            self._update_next_pages()

        page = self._new_pages[0]
        del self._new_pages[0]
        self._cache.append(page)

        return page



    def _update_next_pages(self):
        """ Получаем новый список страниц из поиска, и удаляем из них уже переданные """
        self._find_page_counter += 1
        pages = []

        for i in range(self._find_page_counter):
            pages.append(random.randint(1,100))

        self._new_pages = list(set(pages) - set(self._cache))
        if not self._new_pages:
            print("Список пуст")
            self._update_next_pages()
            time.sleep(2)

        print(self._new_pages)




    def __iter__(self):
        return self


    def __next__(self):
        if self._counter < 1000:
            self._counter += 1
            return self._next_page()
        else:
            raise StopIteration


for i in SimpleIterator():
    print(i)
    time.sleep(2)