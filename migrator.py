import sys
import datetime

from app.db import db
from app.models.user import User
from app.models.bot import Bot
from app.models.bot_stat import BotStat
from app.models.handler import Handler
from app.models.handler_task import HandlerTask
from app.models.handler_task_log import HandlerTaskLog
from app.models.balance import Balance

drop = [Balance, HandlerTaskLog, HandlerTask, Handler, BotStat, Bot, User]
create = [User, Bot, BotStat, Handler, HandlerTask, HandlerTaskLog, Balance]

for model in drop:
    try:
        model.drop_table()
        print("Модель {} удалена".format(model))
    except Exception as ex:
        print("Ошибка удаления модели {}".format(model))
        print(ex)


print()
for model in create:
    model.create_table()
    print("Модель {} создана".format(model))

print()
print("Добавление пользователей")
krupenin, _ = User.get_or_create(username = 'krupenin', defaults = { 'password': '123456aA',
    'name': 'Константин Крупенин', 'role': 'admin' })

print("Добавление аккаунтов")
bot_krupenin, _ = Bot.get_or_create(user=krupenin, name='Константин Крупенин', login='79208791212',
        password='20952095KKs', status='active')
bot_kozirenko, _ = Bot.get_or_create(user=krupenin, name='Юлия Козыренко', login='79605204341',
        password='20952095Kss', status='active')
bot_vlasov, _ = Bot.get_or_create(user=krupenin, name='Евгений Власов', login='79206108171',
        password='20952095Ks', status='active')
bot_filatova, _ = Bot.get_or_create(user=krupenin, name='Светлана Филатова', login='79208980155',
        password='20952095Ks', status='active')

print("Добавление обработчиков")
# Handler.get_or_create(bot = bot_krupenin, name = 'potanenko_parce_youtube', status = 'expected', defaults = { 'options':  {} } )
# Handler.get_or_create(bot = bot_krupenin, name = 'potanenko_parce_site', status = 'expected', defaults = { 'options':  {} } )
# Handler.get_or_create(bot = bot_krupenin, name = 'potanenko_post_in_group', status = 'expected', defaults = { 'options':  {} } )

h, _ = Handler.get_or_create(bot = bot_kozirenko, name = 'posting_in_group', status = 'disabled',
        defaults = { 'options':  {
                'search': 'https://vk.com/search?c[age_from]=18&c[age_to]=35&c[online]=1&c[per_page]=40&c[photo]=1&c[section]=people&c[sex]=2&c[sort]=1&c[status]=1',
                'count': 10,
                'count_like': 5 } },
        last_run = datetime.datetime.now(),
        next_run = datetime.datetime.now(),
        next_step = 1 )

print(h.errors)
Handler.get_or_create(bot = bot_kozirenko, name = 'add_friends', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_kozirenko, name = 'unfolow', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_kozirenko, name = 'like', status = 'enabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_kozirenko, name = 'invite', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_kozirenko, name = 're_invite', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_kozirenko, name = 'posting_in_topick', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )


Handler.get_or_create(bot = bot_filatova, name = 'add_friends', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_filatova, name = 'unfolow', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_filatova, name = 'like', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_filatova, name = 'invite', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_filatova, name = 're_invite', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_filatova, name = 'posting_in_topick', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )


Handler.get_or_create(bot = bot_vlasov, name = 'add_friends', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_vlasov, name = 'unfolow', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_vlasov, name = 'like', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_vlasov, name = 'invite', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_vlasov, name = 're_invite', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_vlasov, name = 'posting_in_topick', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )
Handler.get_or_create(bot = bot_vlasov, name = 'mass_report', status = 'disabled',
        defaults = { 'options':  {} }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )


Handler.get_or_create(bot = bot_krupenin, name = 'update_city', status = 'disabled',
        defaults = { 'options':  { 'count': 5 } }, last_run = datetime.datetime.now(), next_run = datetime.datetime.now(), next_step = 1 )