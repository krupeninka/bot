from datetime import datetime

from peewee import CharField, PrimaryKeyField, BooleanField, ForeignKeyField, IntegerField, DateTimeField
from flask_login import UserMixin
from playhouse.signals import pre_save

from app.models._base import BaseModel
from app.models.handler import Handler


class HandlerTask(BaseModel):
    """ Исполнение обработки """

    id = PrimaryKeyField()
    handler = ForeignKeyField(Handler, index=True, related_name='tasks')
    created_at = DateTimeField(index=True)
    finished_at = DateTimeField(index=True, null=True)


@pre_save(sender=HandlerTask)
def handler_task_set_created_at(model_class, instance, created):
    """
    Автоматически ставим время создания исполнения
    """
    if not instance.created_at:
        instance.created_at = datetime.now()
