import hashlib

from peewee import CharField, PrimaryKeyField, BooleanField
from flask_login import UserMixin
from playhouse.signals import pre_save

from app.models._base import BaseModel


class User(BaseModel, UserMixin):
    """ Пользователи системы """

    id = PrimaryKeyField()
    username = CharField(unique=True, index=True)
    encrypted_password = CharField(default='')
    name = CharField()
    active = BooleanField(null=False, default='t')
    role = CharField(null=False, default='user')

    password = None

    def authenticate(self, password):
        """Проверка пароля"""

        return self.encrypted_password == encrypt(password)


def encrypt(password):
    """Шифрование пароля"""
    password += 'salt'
    password = password.encode('utf-8')
    return hashlib.sha1(password).hexdigest()


@pre_save(sender=User)
def user_set_encrypted_password(model_class, instance, created):
    """Если пароль установлен, шифруем его и записываем в encrypted_password"""
    if len(instance.password or "") == 0:
        return

    instance.encrypted_password = encrypt(instance.password)
