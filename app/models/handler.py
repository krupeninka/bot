import hashlib

from peewee import CharField, PrimaryKeyField, BooleanField, ForeignKeyField, IntegerField, DateTimeField
from playhouse.postgres_ext import JSONField
from playhouse.signals import post_init

from app.flask import app
from app.models._base import BaseModel
from app.models.bot import Bot


HANDLERS = {
    # Общие
    'like': { 'name': 'Лайки',  'use': 'common', 'handler': 'LikeHandler', 'module': 'like_handler',
        'options': {
            'search': { 'name': 'Поисковой фильтр', 'default': 'https://vk.com/search' },
            'count': { 'name': 'Количество людей', 'default': 1 },
            'count_like': { 'name': 'Количество лайков', 'default': 1 }}},
    'invite': { 'name': 'Приглашение в группу', 'use': 'common', 'handler': 'InviteHandler', 'module': 'invite_handler',
        'options': {
            'search': {  'name': 'Поисковой фильтр', 'default': 'https://vk.com/friends?act=invite&group_id=1'}}},
    're_invite': { 'name': 'Ответ на приглашение', 'use': 'common', 'handler': 'ReInviteHandler', 'module': 're_invite_handler',
        'options': {
            'groups': { 'name': 'Адреса и ID групп', 'default': 'https://vk.com/apiclub#1\nhttps://vk.com/club3#3' },
            'message': { 'name': 'Сообщение', 'default': 'Добрый день' },
            'add_message': {'name': 'Дополнительное сообщение', 'default': 'Добрый день' }
        }},
    'add_friends': { 'name': 'Добавление друзей', 'use': 'common', 'handler': 'AddFriendHandler', 'module': 'add_friend_handler',
        'options': {}},
    'unfolow': { 'name': 'Отписаться', 'use': 'common', 'handler': 'UnfolowHandler', 'module': 'unfolow_handler',
        'options': {}},
    'posting_in_group': { 'name': 'Постинг в группу', 'use': 'common', 'handler': 'PostingInGroupHandler', 'module': 'posting_in_group_handler',
        'options': {
            'messages': { 'name': 'Сообщения', 'default': 'Добрый день' },
            'count': { 'name': 'Количество', 'default': 5 }
        }},
    'posting_in_topick': { 'name': 'Постинг в топик', 'use': 'common', 'handler': 'PostingInTopickHandler', 'module': 'posting_in_topick_handler',
        'options': {}},
    'repost': { 'name': 'Постинг в топик', 'use': 'common', 'handler': 'RepostHandler', 'module': 'repost',
        'options': {}},
    'mass_report': { 'name': 'Массовая рассылка', 'use': 'common', 'handler': 'MassReportHandler', 'module': 'mass_report_handler',
        'options': {
                'mass_report_id': { 'name': 'Массовая рассылка', 'default': '' },
                'max_count': {'name': "Максимальное количество", 'default': '20'}}},
    'mass_report_friends': { 'name': 'Массовая рассылка Друзьям', 'use': 'common', 'handler': 'MassReportFriendsHandler', 'module': 'mass_report_handler',
        'options': {
            'search': { 'name': 'Поисковой фильтр', 'default': 'https://vk.com/search' },
            'list': { 'name': 'Список пользователей которым', 'default': 'https://vk.com/search' },
            'messages': { 'name': 'Сообщения', 'default': 'Привет!' },
            'count': { 'name': 'Количество', 'default': '1' }}},
    # Специальные
    'update_city': { 'name': 'Обновление городов', 'use': 'special', 'handler': 'UpdateCityHandler', 'module': 'update_city_handler',
        'options': {
            'count': { 'name': 'Количество', 'default': '25' }}},
    'update_darom': { 'name': 'Обновление Даром', 'use': 'special', 'handler': 'UpdateDaromHandler', 'module': 'update_darom_handler',
        'options': {
            'count': { 'name': 'Количество', 'default': '25' }}},
    'update_dobav': { 'name': 'Обновление Добавь', 'use': 'special', 'handler': 'UpdateDobavHandler', 'module': 'update_dobav_handler',
        'options': {}}
    }


HANDLER_STATUSES = {
    'disabled': 'Отключен',
    'enabled': 'Включен',
    'expected': 'Ожидается'}


class Handler(BaseModel):
    """ Обработка для аккаунтов """

    id = PrimaryKeyField()
    bot = ForeignKeyField(Bot, index=True, related_name='handlers')
    name = CharField()
    status = CharField()
    options = JSONField()
    last_run = DateTimeField(index=True, null=True)
    next_run = DateTimeField(index=True, null=True)
    next_step = IntegerField(null=True)

    validates = {
            'next_step': {
                    'presence': { 'message': 'Время повторения не может быть пустым'},
                    'numerical': { 'message': 'Время повторения должно быть целым числом от 1 до 1440', 'only_integer': True, 'greater': 1, 'less': 1440 } } }


    @property
    def module(self):
        """ Модуль который нужно подключить для получения обработчика """
        try:
            return HANDLERS[self.name]['module']
        except KeyError:
            return "Не найдено"


    @property
    def handler(self):
        """ Модуль который нужно подключить для получения обработчика """
        try:
            return HANDLERS[self.name]['handler']
        except KeyError:
            return "Не найдено"


    def raw_name(self):
        """ Название обработки """
        try:
            return HANDLERS[self.name]['name']
        except KeyError:
            return "Не найдено"


    def raw_status(self):
        """ Расшифровка статуса """
        try:
            return HANDLER_STATUSES[self.status]
        except KeyError:
            return "Не найден"


    def raw_option(self, key):
        """ Расшифровка ключа опции """
        try:
            return HANDLERS[self.name]['options'][key]['name']
        except KeyError:
            return "Не найдено"


    def value_option(self, key):
        """ Значение ключа опции """
        try:
            return self.options[key]
        except KeyError:
            return "Не найдено"


    def default_value_option(self, key):
        """ Значение по умолчанию ключа опции """
        try:
            return HANDLERS[self.name]['options'][key]['default']
        except KeyError:
            return "Не найдено"


    def options_position(self):
        """ Список ключей опций в опеределенном порядке """
        try:
            return HANDLERS[self.name]['options_position']
        except KeyError:
            return []


@post_init(sender=Handler)
def handler_set_options(model_class, instance):
    """Автоматически формируем словарь options, для удобства обработки"""
    if instance.name:
        for key in HANDLERS[instance.name].get('options').keys():
            instance.options[key] = instance.options.get(key) or instance.default_value_option(key)
