from peewee import CharField, PrimaryKeyField, ForeignKeyField

from app.models._base import BaseModel
from app.models.user import User


class Balance(BaseModel):
    """ Балансе на социальных биржах """

    id = PrimaryKeyField()
    user = ForeignKeyField(User, index=True, related_name='balances')
    site = CharField()
    login = CharField(null=True)
    password = CharField(null=True)
    driver = CharField()
    amount = CharField(null=True)
