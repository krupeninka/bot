from datetime import datetime

from peewee import CharField, PrimaryKeyField, DateTimeField
from playhouse.signals import pre_save

from app.models._base import BaseModel


class Cache(BaseModel):
    """ Информация по отправленым приглашениям """

    id = PrimaryKeyField()
    key = CharField(index=True)
    created_at = DateTimeField(index=True)


@pre_save(sender=Cache)
def cache_set_created_at(model_class, instance, created):
    """ Автоматически ставим время создания исполнения """
    if not instance.created_at:
        instance.created_at = datetime.now()
