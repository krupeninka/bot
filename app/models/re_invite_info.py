from datetime import datetime

from peewee import CharField, PrimaryKeyField, BooleanField, ForeignKeyField, IntegerField, DateTimeField, TextField
from playhouse.signals import pre_save

from app.models._base import BaseModel
from app.models.handler_task import Handler


STATUSES = {
    'bot_еnter': 'Бот вступил',
    'send_message': 'Отправлено сообщение',
    'bot_out'
    'user_еnter': 'Пользователь вступил',
    'user_out': 'Пользователь вышел',
    'bot_out_to': 'Бот вышел в ответ'}

# Логика переходов
# Бот вступил - Отправлено сообщение - Пользователь вступил
#                                     \                     \
#                                      \                     Пользователь вышел - Бот вышел в ответ
#                                       \
#                                        Бот не дождался

class ReInviteInfo(BaseModel):
    """ Информация по ответам на приглашения """

    id = PrimaryKeyField()
    status = CharField(index=True)
    handler_task = ForeignKeyField(Handler, index=True, related_name='logs')
    user_id = CharField(index=True)
    group_id = CharField(index=True)

    created_at = DateTimeField(index=True)


@pre_save(sender=HandlerTaskLog)
def handler_task_log_set_created_at(model_class, instance, created):
    """ Автоматически ставим время создания исполнения """
    if not instance.created_at:
        instance.created_at = datetime.now()
