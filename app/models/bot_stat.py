from peewee import CharField, PrimaryKeyField, ForeignKeyField, IntegerField, DateField

from app.models._base import BaseModel
from app.models.bot import Bot

KEYS = {
    'like': 'Лайки',
    'unfolow': 'Отписаться',
    'add_friend': 'Подтвердить друзей',
    'like': 'Лайли',
    'invite': 'Приглашения в группу',
    're_invite': 'Ответ на приглашение',
    'posting_in_group': "Постинг в группу",
    'mass_report': "Массовая рассылка",
    'update_darom': "Обновление даром",
    'update_city': 'Обновление городов',
    'update_dobav': 'Обновление Добавь'}


class BotStat(BaseModel):
    """ Аккаунты социальных сетей """

    id = PrimaryKeyField()
    bot = ForeignKeyField(Bot, index=True, related_name='stats')
    date = DateField(index=True)
    key = CharField(index=True)
    value = IntegerField()


    def raw_key(self):
        """ Локализация статуса """
        return KEYS.get(self.key, self.key)
