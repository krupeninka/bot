from collections import OrderedDict

from peewee import CharField, PrimaryKeyField, TextField, IntegerField, DateTimeField, TextField
from playhouse.postgres_ext import JSONField

from app.models._base import BaseModel


STATUSES = OrderedDict([
    ('new', 'Новая'),
    ('open_wall', 'Стена открыта'),
    ('close_wall', 'Стена закрыта'),
    ('close_group', 'Закрытая группа'),
    ('error', 'Не найдена'),
    ('exclude', "Исключена")
])


class Dobav(BaseModel):
    """ Список групп, в котрые можно постить объявления """

    id = PrimaryKeyField()
    status = CharField()
    name = CharField()
    url = CharField(index=True)
    count = IntegerField(index=True)
    updated_at = DateTimeField(index=True)
    info = TextField(null=True)


    def raw_status(self):
    	""" Локализация статуса """
    	try:
    		return STATUSES[self.status]
    	except:
    		return self.status
