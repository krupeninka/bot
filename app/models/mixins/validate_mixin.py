# coding: utf-8

import re

from app.flask import app

class ValidateMixin():
    """
    Класс позволяющий валидировать модель перед ее сохранением в базу данных, с использованием
    определенных правил.

    Синтаксис:
        validates = {
                'атрибут': { 'правило': { 'Параметр': "Значение", ... }, ...},
                ... }

    Пример:
        validates = {
            'name': { 'require': True, 'length': { min: 3, 'max': 15, 'message': 'Имя не может быть пустым' } } }

    Общие параметры:
        - message - сообщение которое будет выводится если валидация не пройдена
        - if - принимает название метода, правила пропускаются если результат метода положительный
        - unless - принимает название метода, правила пропускаются если результат метода отрицательный
        - allow_blank - правила пропускаются если значения равно None или пустая строка

    Правила:

    presence - проверка наличая значения у атрибута(может не принимать параметры)
            - message - сообщение которое будет выводится если валидация не пройдена

    length  - длинна параметра, применяется только для строковых значений
        - min - минимальное значение
        - max - максимальное значение
        - in - длинна находится в промежутке range() или списке list()
        - is - определенной длинны


    numerical -
        - only_integer - только целое значение
        - greater - больше или равно указанному значению
        - equal - равно указанному значению
        - less - меньше или равно указанному значению

    format - формат
        - with - регулярное выражение с которому должно соотверствовать значение атрибута
        - without - регулярное выражение с которому не должно соотверствовать значение атрибута

    #TODO:
    uniqueness - проверка уникальности значения
        - with - поле или поля которые должны составлять уникальное значение

    inclusion - значение атрибута включает должно быть в списке in или не быть в списке out
        - in - список в который долно входить значение
        - out - список в которое не должно входить значение
    """
    errors = {}
    validates = {}


    def valid(self):
        """
        Метод который проверяет валиднось экземпляра класса
        """
        if not self.validates:
            return True

        self.errors = {}

        self._validation()
        if self.errors == {}:
            return True
        else:
            return False


    def _validation(self):
        """
        Метод перебирает правила валидации. При невыполнении правил добавляет сообщения в словарь
        errors, распределяя их по атрибутам.
        """
        for attr, rules in self.validates.items():
            for rule, options in rules.items():
                if options.get('allow_blank'):
                    if getattr(self, attr) == None and getattr(self, attr) == "":
                        continue

                if options.get('if'):
                    if not getattr(self, options.get('if'))():
                        continue

                if options.get('unless'):
                    if getattr(self, options.get('unless'))():
                        continue

                getattr(self, "_%s_validation" % rule)(attr, options)



    def _add_error(self, attr, message):
        """Добавление ошибки для определенного методов"""
        if not self.errors.get(attr):
            self.errors[attr] = []

        self.errors[attr].append(message)


    def _presence_validation(self, attr, options):
        """проверка наличая значения у атрибута"""
        if getattr(self, attr) == "" or getattr(self, attr) == None:
            message = options.get("message") or "%s can not be blank" % attr.title()
            self._add_error(attr, message)


    def _length_validation(self, attr, options):
        """Проверка длинны аттрибута"""
        value = getattr(self, attr)
        length = len(value or "")
        _min = options.get('min')
        _max = options.get('max')
        _in = options.get('in')
        _is = options.get('is')

        if _min and _max:
            if length < _min or length > _max:
                message = options.get('message') or attr.title() + ' should be between %s and %s characters' % (_min, _max)
        elif _min:
            if length < _min:
                message = options.get('message') or attr.title() + ' must be less than %s characters' % _min
        elif _max:
            if length > _max:
                message = options.get('message') or attr.title() + ' should be greater %s characters' % _max
        elif _in:
            if not length in _in:
                message = options.get('message') or attr.title() + ' should be to %s characters' % (_min, _max)
        elif _is:
            if not length == _is:
                message = options.get('message') or attr.title() + '  should be equal to the ' + ", ".join(_is)

        if 'message' in locals().keys():
            self._add_error(attr, message)


    def _numerical_validation(self, attr, options):
        """Проверка  числового типа и велечины значения аттрибута"""
        value = getattr(self, attr)
        _only_integer = options.get('only_integer', False)
        _greater = options.get('greater')
        _less = options.get('less')
        _equal = options.get('equal')

        if type(value) not in (int, float):
            try:
                if '.' in value:
                    value = float(value)
                else:
                    value = int(value)
            except (ValueError, TypeError):
                message = options.get('message') or attr.title() + ' has an invalid format'
                self._add_error(attr, message)
                return

        if _only_integer:
            if type(value) is not int:
                message = options.get('message') or attr.title() + ' must be an integer'

        if _equal:
            if _equal > value:
                message = options.get('message') or attr.title() + ' must be equal to %s' % _equal

        if _greater and _greater > value:
            message = options.get('message') or attr.title() + ' must be greater than %s' % _greater

        if _less and _less < value:
            message = options.get('message') or attr.title() + ' must be less than two %s' % _less

        if 'message' in locals().keys():
            self._add_error(attr, message)


    def _format_validation(self, attr, options):
        """ Проверка аттрибута по регулярному выродению """
        value = getattr(self, attr)
        _with = options.get('with', None)
        _without = options.get('without', None)

        if _with and re.match(_with, str(value or "")) is None:
            message = options.get('message') or attr.title() + ' has the wrong format'

        if _without and not re.match(_with, str(value or "")) is None:
            message = options.get('message') or attr.title() + ' has the wrong format'

        if 'message' in locals().keys():
            self._add_error(attr, message)
