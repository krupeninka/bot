from peewee import CharField, PrimaryKeyField, ForeignKeyField, TextField, IntegerField, DateTimeField
from playhouse.postgres_ext import JSONField

from app.models._base import BaseModel
from app.models.city import City


STATUSES = {
    'new': 'Новая',
    'active': 'Активная',
    'present': 'Есть моя',
    'frozen': 'Заблокирован'
}


class Darom(BaseModel):
    """ Информация о конкурентах, в которых есть группы даром """

    id = PrimaryKeyField()
    city = ForeignKeyField(City, index=True, related_name='daroms')
    status = CharField()
    max_count = IntegerField(index=True)
    group_count = IntegerField(index=True)
    count = IntegerField(index=True)
    persent = IntegerField(index=True)
    updated_at = DateTimeField(index=True)
    options = JSONField()


    def raw_status(self):
    	""" Локализация статуса """
    	try:
    		return STATUSES[self.status]
    	except:
    		return self.status
