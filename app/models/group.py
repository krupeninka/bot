from peewee import CharField, PrimaryKeyField, ForeignKeyField, TextField, IntegerField

from app.models._base import BaseModel
from app.models.user import User


class Group(BaseModel):
    """ Аккаунты социальных сетей """

    id = PrimaryKeyField()
    user = ForeignKeyField(User, index=True, related_name='groups')
    name = CharField()
    url = CharField()
    group_id = IntegerField(index=True)
    count = IntegerField(index=True)
    info = TextField(default='')

    validates = {
            'url': {
                    'presence': { 'message': 'Адрес группы не может быть пустым'} },
            'name': {
                    'presence': { 'message': 'Название группы не может быть пустым'} },
            'user': {
                    'presence': { 'message': 'Пользователь не может быть пустым'} },
            'group_id': {
                    'presence': { 'message': 'ID группы не может быть пустым'} },
            'count': {
                    'presence': { 'message': 'Количество в группе не может быть пустым'},
                    'numerical': { 'message': 'Количество в группе должно быть числом', 'only_integer': True } } }
