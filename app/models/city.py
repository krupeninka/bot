from peewee import CharField, PrimaryKeyField, IntegerField, DateTimeField

from app.models._base import BaseModel
from app.models.user import User
from playhouse.postgres_ext import JSONField


class City(BaseModel):
    """ Аккаунты социальных сетей """

    id = PrimaryKeyField()
    name = CharField()
    count = IntegerField(index=True)
    updated_at = DateTimeField(index=True)
    options = JSONField()
