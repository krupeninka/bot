from datetime import datetime

from peewee import CharField, PrimaryKeyField, BooleanField, ForeignKeyField, IntegerField, DateTimeField
from peewee import DateField, TextField
from playhouse.signals import pre_save

from app.models._base import BaseModel
from app.models.mass_report import MassReport
from app.models.bot import Bot

STATUSES = {
    'enabled': 'Включена',
    'disabled': 'Отключена'}



class MassReportInfo(BaseModel):
    """ Массовая рассылка """

    id = PrimaryKeyField()

    mass_report_id = IntegerField(index=True)
    bot_id = IntegerField(index=True)
    user_id = CharField(index=True)
    created_at = DateTimeField()


@pre_save(sender=MassReportInfo)
def mass_report_info_set_created_at(model_class, instance, created):
    """ Автоматически ставим время создания исполнения """
    if not instance.created_at:
        instance.created_at = datetime.now()
