from playhouse.signals import Model

from app.db import db
from app.models.mixins.validate_mixin import ValidateMixin

class BaseModel(Model, ValidateMixin):
    """Базвый класс, от которого наседуются все модели приложения"""

    class Meta:
        database = db


    def is_new(self):
        """Проверяем сохранялась или нет запись в базу"""
        return None == self.id

    def is_new(self):
        """Проверяем сохранялась или нет запись в базу"""
        return None == self.id


    def save(self, force_insert=False, only=None):
        """При сохранении проверяем валидность модели. Если ошибок нет возвращаем True"""
        if self.valid():
            super(BaseModel, self).save(force_insert=force_insert, only=only)
            return True

        return False
