from peewee import CharField, PrimaryKeyField, ForeignKeyField, TextField

from app.models._base import BaseModel
from app.models.user import User


BOT_STATUSES = {
    'active': 'Активен',
    'stoped': 'Остановлен',
    'frozen': 'Заблокирован'
}


class Bot(BaseModel):
    """ Аккаунты социальных сетей """

    id = PrimaryKeyField()
    user = ForeignKeyField(User, index=True, related_name='bots')
    name = CharField()
    login = CharField()
    password = CharField()
    status = CharField()
    info = TextField(default='')


    validates = {
            'user': {
                    'presence': { 'message': 'Владелец не может быть пустым'}},
            'name': {
                    'presence': { 'message': 'Имя не может быть пустым'}},
            'login': {
                    'presence': { 'message': 'Логин не может быть пустым'}},
            'password': {
                    'presence': { 'message': 'Пароль не может быть пустым'}},
            'status': {
                    'presence': { 'message': 'Status не может быть пустым'}}}


    def raw_status(self):
    	""" Локализация статуса """
    	try:
    		return BOT_STATUSES[self.status]
    	except:
    		return self.status
