from datetime import datetime

from peewee import CharField, PrimaryKeyField, ForeignKeyField, DateTimeField
from playhouse.signals import pre_save

from app.models._base import BaseModel
from app.models.bot import Bot


STATUSES = {
    'invite': 'Отправлено',
    'user_еnter': 'Пользователь вступил',
    'user_out': 'Пользователь вышел',
    'ignore': 'Проигнорировал'}


class InviteInfo(BaseModel):
    """ Информация по отправленым приглашениям """

    id = PrimaryKeyField()
    status = CharField(index=True)
    bot = ForeignKeyField(Bot, index=True, related_name='invite_infos')
    user_id = CharField(index=True)
    group_id = CharField(index=True)
    created_at = DateTimeField(index=True)


@pre_save(sender=InviteInfo)
def invite_info_set_created_at(model_class, instance, created):
    """ Автоматически ставим время создания исполнения """
    if not instance.created_at:
        instance.created_at = datetime.now()


@pre_save(sender=InviteInfo)
def invite_info_set_status(model_class, instance, created):
    """ Автоматически ставим время создания исполнения """
    if not instance.status:
        instance.status = 'invite'
