from peewee import CharField, PrimaryKeyField, BooleanField, ForeignKeyField, IntegerField
from peewee import DateField, TextField

from app.models._base import BaseModel

STATUSES = {
    'enabled': 'Включена',
    'disabled': 'Отключена'}



class MassReport(BaseModel):
    """ Массовая рассылка """

    id = PrimaryKeyField()
    name = CharField(null=True)
    messages = TextField()
    status = CharField(index=True, null=True)
    url = CharField()


    validates = {
            'name': {
                    'presence': { 'message': 'Название не может быть пустым'}},
            'messages': {
                    'presence': { 'message': 'Сообщения не может быть пустым'}},
            'status': {
                    'presence': { 'message': 'Статус не может быть пустым'}},
            'url': {
                    'presence': { 'message': 'Поисковая строка не может быть пустым'}}}


    def raw_status(self):
        """ Расшифровка статуса """
        try:
            return STATUSES[self.status]
        except KeyError:
            return "Не найден"
