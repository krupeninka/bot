from datetime import datetime

from peewee import CharField, PrimaryKeyField, BooleanField, ForeignKeyField, IntegerField, DateTimeField, TextField
from playhouse.signals import pre_save

from app.models._base import BaseModel
from app.models.handler_task import HandlerTask


STATUSES = {
    'success': "Успешний",
    'error': 'Ошибка',
    'critical': 'Критическая' }


class HandlerTaskLog(BaseModel):
    """ Исполнение обработки """

    id = PrimaryKeyField()
    status = CharField()
    handler_task = ForeignKeyField(HandlerTask, index=True, related_name='logs')
    message = TextField()
    traceback = TextField(null=True)
    created_at = DateTimeField(index=True)


    # @classmethod
    def is_success(self):
        """ Фильтр по статусу 'Успешный'"""
        self.where(self.__class__.status == 'success')


@pre_save(sender=HandlerTaskLog)
def handler_task_log_set_created_at(model_class, instance, created):
    """ Автоматически ставим время создания исполнения """
    if not instance.created_at:
        instance.created_at = datetime.now()
