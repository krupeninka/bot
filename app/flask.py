import os
import logging

from flask import Flask
from flask_login import LoginManager
from werkzeug import ImmutableDict
from logging import Formatter

from app import root

class FlaskHaml(Flask):
    """Обертка на Flask которая позволяет работать с haml"""

    jinja_options = ImmutableDict(
        extensions=['jinja2.ext.autoescape', 'jinja2.ext.with_', 'hamlish_jinja.HamlishExtension']
    )


# Запуск приложения
# app = FlaskHaml(__name__, instance_relative_config=True, static_folder='assets')
app = FlaskHaml(__name__, instance_relative_config=True)
app.secret_key = '4689863de94441ea86744be632d12dbb93bab07d07584ccfb82cf1ce59b94d49'

# Авторизация пользователей
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "sessions_new"
