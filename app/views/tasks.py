from flask import render_template, request, redirect
from flask_login import login_required, current_user

from app.flask import app

from app.models.handler import Handler
from app.models.handler_task import HandlerTask
from app.models.handler_task_log import HandlerTaskLog


@app.route('/tasks/<id>/log')
@login_required
def tasks_log(id):
    task = HandlerTask.get(HandlerTask.id == id)
    return render_template('tasks/log.haml', task=task, log_by_status=_log_by_status)


@app.route('/tasks/<id>/destroy')
@login_required
def tasks_destroy(id):
    try:
        task = HandlerTask.get(HandlerTask.id == id)
        handler_id = task.handler.id
        HandlerTaskLog.delete().where(HandlerTaskLog.handler_task == task).execute()
        task.delete_instance()
    except HandlerTask.DoesNotExist:
        return redirect("handlers")
    return redirect("handlers/" + str(handler_id))


def _log_by_status(task, status):
    """ Полученение логов по статусу """

    return task.logs.where(HandlerTaskLog.status == status)