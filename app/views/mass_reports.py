from datetime import date

from flask import request, render_template, redirect, url_for, flash
from flask_login import login_required, current_user

from app.flask import app

from app.views._form_helper import FormHelper
from app.models.mass_report import MassReport, STATUSES

#foo = 'foo'

@app.route('/mass_reports')
@login_required
def mass_reports_index():
    reports = MassReport.select().order_by(MassReport.id.desc())
    return render_template('mass_reports/index.haml', reports=reports)


@app.route('/mass_reports/new')
@login_required
def mass_reports_new():
    report = _build_from_form(None, None)
    helper = FormHelper(report, '/mass_reports')
    return render_template('mass_reports/_form.haml', report=report, helper=helper, statuses=STATUSES)


@app.route('/mass_reports', methods=['POST'])
@login_required
def mass_reports_create():
    report = _build_from_form(None, request.form)
    helper = FormHelper(report, form_url='/mass_reports')

    if report.save():
        flash(u"Создана новая рассылка: {}".format(report.name), 'success')
        return redirect(url_for('mass_reports_show', id = report.id))
    else:
        return render_template('mass_reports/_form.haml', helper=helper, statuses=STATUSES)


@app.route('/mass_reports/<int:id>')
@login_required
def mass_reports_show(id):
    report = MassReport.get(MassReport.id == id)
    return render_template('mass_reports/show.haml', report=report)


@app.route('/mass_reports/<int:id>/edit')
@login_required
def mass_reports_edit(id):
    report = _build_from_form(id, None)
    helper = FormHelper(report, '/mass_reports/{}'.format(report.id))
    return render_template('mass_reports/_form.haml', helper=helper, statuses=STATUSES)


@app.route('/mass_reports/<int:id>', methods=["POST"])
@login_required
def mass_reports_update(id):
    report = _build_from_form(id, request.form)
    helper = FormHelper(report, '/mass_reports/{}'.format(id))

    if report.save():
        flash(u"Обновлена рассылка: {}".format(report.name), 'success')
        return redirect(url_for('mass_reports_show', id = report.id))
    else:
        return render_template('mass_reports/_form.haml', helper=helper, statuses=STATUSES)


def _build_from_form(id, form):
    """ Инициализация экземпляра на основе формы """
    if id:
        report = MassReport.get(id=id)
    else:
        report = MassReport(status='disabled')

    if form:
        report.name = form.get('name', report.name)
        report.messages = form.get('messages', report.messages)
        report.status = form.get('status', report.status)
        report.url = form.get('url', report.url)

    return report
