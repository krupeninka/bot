# coding: utf-8

from flask import render_template
from flask_login import login_required

from app.flask import app

@app.route('/')
@login_required
def main_index():
    return render_template('main/index.haml')
