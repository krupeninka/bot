class FormHelper(object):
    """ Класс хелпер, который позволяет упростить построение форм """

    def __init__(self, instance, form_url):
        self.instance = instance
        self.form_url = form_url


    def input(self, col, label, key, value=None, disabled=''):
        """ HTML input тег """
        value = value or getattr(self.instance, key) or ''

        h = '<div class="col-lg-{col}">'\
            '<div class="form-group">'\
            '<label class="control-label" for="{key}">{label}</label>'\
            '<input class="form-control" id="{key}" name="{key}" type="text" value="{value}" {disabled}>'\
            '</div>'\
            '</div>'

        return h.format(col=col, key=key, label=label, value=value, disabled=disabled)


    def select(self, col, label, key, options, value=None, disabled=''):
        """ HTML select тег """
        value = value or getattr(self.instance, key)

        h = '<div class="col-lg-{col}">'\
            '<div class="form-group">'\
            '<label class="control-label" for="{key}">{label}</label>'\
            '<select class="form-control" id="{key}" name="{key}">'
        for k, v in options.items():
            if v == value:
                h += '<option value="{key}" selected="selected">{value}</option>'.format(key=k, value=v)
            else:
                h += '<option value="{key}">{value}</option>'.format(key=k, value=v)

        h += '</select></div></div>'

        return h.format(col=col, key=key, label=label, value=value)


    def textarea(self, col, label, key, value=None, rows = 1, disabled=''):
        """ HTML input тег """
        value = value or getattr(self.instance, key) or ''

        h = '<div class="col-lg-{col}">'\
            '<div class="form-group">'\
            '<label class="control-label" for="{key}">{label}</label>'\
            '<textarea class="form-control" id="{key}" name="{key}" rows="{rows}">{value}</textarea>'\
            '</div>'\
            '</div>'

        return h.format(col=col, key=key, label=label, value=value, rows=rows)
