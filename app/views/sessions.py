# coding:utf-8

from flask import request, render_template, redirect, url_for
from flask_login import current_user, login_user, logout_user

from app.flask import app, login_manager
from app.models.user import User


@login_manager.user_loader
def load_user(id):
    try:
        return User.get(User.id == int(id))
    except User.DoesNotExist:
        return None


@app.route('/sessions/new')
def sessions_new():
    if current_user.is_authenticated:
        return redirect('/')
    return render_template('sessions/new.haml')


@app.route('/sessions', methods=['POST'])
def sessions_create():
    if request.form.get('user[login]') and request.form.get('user[password]'):
        try:
            user = User.get(User.username == request.form.get('user[login]'))
        except User.DoesNotExist:
            return redirect(url_for('sessions_new'))

        if user.authenticate(request.form['user[password]']):

            login_user(user)
            return redirect(url_for('main_index'))

    return redirect(url_for('sessions_new'))

@app.route('/sessions/delete')
def sessions_delete():
    logout_user()

    return redirect('/sessions/new')
