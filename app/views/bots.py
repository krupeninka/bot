from datetime import date, timedelta

from flask import request, render_template, redirect, url_for, flash
from flask_login import login_required, current_user
from peewee import fn

from app.flask import app
from app.models.bot import Bot
from app.models.bot_stat import BotStat, KEYS
from app.models.handler import Handler
from app.models.handler_task_log import HandlerTaskLog


@app.route('/bots')
@login_required
def bots_index():
    bots = Bot.select().where(Bot.user_id == current_user.id).order_by(Bot.id.asc())
    return render_template('bots/index.haml', bots=bots)


@app.route('/bots/new')
@login_required
def bots_new():
    bot = _build_from_form(None, None)
    return render_template('bots/new.haml', bot=bot, url='/bots')


@app.route('/bots', methods=['POST'])
@login_required
def bots_create():
    bot = _build_from_form(None, request.form)

    if bot.save():
        flash(u"Создан новый бот: {}".format(bot.name), 'success')
        return redirect(url_for('bots_show', id = bot.id))
    else:
        return render_template('bots/new.haml', bot=Bot())


@app.route('/bots/<int:id>')
@login_required
def bots_show(id):
    bot = Bot.get(Bot.id == id)
    stats = _get_stats(bot)
    handlers = bot.handlers.order_by(Handler.next_run.asc())
    return render_template('bots/show.haml', bot=bot, stats=stats, error_helper = _errors_helper,
        handlers=handlers)


@app.route('/bots/<int:id>/edit')
@login_required
def bots_edit(id):
    bot = _build_from_form(id, None)
    return render_template('bots/edit.haml', bot=bot, url='/bots/{}'.format(bot.id))


@app.route('/bots/<int:id>', methods=["POST"])
@login_required
def bots_update(id):
    bot = _build_from_form(id, request.form)

    if bot.save():
        flash(u"Обновлены данные бота: {}".format(bot.name), 'success')
        return redirect(url_for('bots_show', id = bot.id))
    else:
        return render_template('bots/edit.haml', bot=bot, url='/bots/{}'.format(id))


def _build_from_form(id, form):
    """ Инициализация экземпляра на основе формы """
    if id:
        bot = Bot.get(id=id)
    else:
        bot = Bot(user=current_user.id)

    if form:
        bot.name = form.get('name', bot.name) or ''
        bot.login = form.get('login', bot.login) or ''
        bot.password = form.get('password', bot.password) or ''
        bot.status = form.get('status', bot.status) or ''
        bot.info = form.get('info', bot.info) or ''

    return bot


def _errors_helper(handler):
    """ Хелпер для подсчета ошибок для обработкам """
    return HandlerTaskLog.select(fn.COUNT('*').alias('count'))\
        .where(HandlerTaskLog.handler_task << handler.tasks, HandlerTaskLog.status == 'critical' )\
        .get().count


def _get_stats(bot):
    """ Формирование статистики для бота, за три последни дня """
    result = { 'days': [], 'values': {} }

    for index in range(7):
        day = str(date.today() - timedelta(days = index))
        result['days'].append(day)
        stats = bot.stats.where(BotStat.date == day)

        for stat in stats:
            key = KEYS.get(stat.key, stat.key)

            if not result['values'].get(key):
                result['values'][key] = [None, None, None, None, None, None, None]

            result['values'][key][index] = stat.value

    return result
