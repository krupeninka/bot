from flask import render_template, redirect, request, url_for, flash
from flask_login import login_required, current_user

from app.flask import app

from app.models.dobav import Dobav, STATUSES
from app.views._form_helper import FormHelper



@app.route('/dobavs')
@login_required
def dobavs_index():
    filter = _filter(request.args)
    dobavs = _filter_dobavs(filter)
    return render_template('dobavs/index.haml', dobavs=dobavs, filter=filter)


@app.route('/dobavs/<int:id>/edit')
@login_required
def dobavs_edit(id):
    dobav = _build_from_form(id, None)
    helper = FormHelper(dobav, '/dobavs')
    return render_template('dobavs/form.haml', dobav=dobav, url='/dobavs/{}'.format(id), helper=helper, statuses=STATUSES)


# @app.route('/dobavs/<int:id>', methods=['POST'])
# @login_required
# def dobavs_update(id):
#     dobav = _build_from_form(id, request.form)

#     # TODO: Нужно это вынести в миксин валидации. Сделаю это как-нибудь позже
#     if not _uniqueness_dobav(dobav):
#         flash(u"Адрес группы уже есть в базе", 'danger')
#         return render_template('dobavs/form.haml', dobav=dobav)
#     if dobav.save():
#         flash(u"Группа успешно обновлена: {}".format(dobav.name), 'success')
#         return redirect(url_for('dobavs_index'))
#     else:
#         return render_template('dobavs/form.haml', dobav=dobav)


@app.route('/dobavs/<int:id>/delete')
@login_required
def dobavs_delete(id):
    dobav = Dobav.get(id=id)
    dobav.delete_instance()
    flash(u"Группа успешно удалена: {}".format(dobav.name), 'success')
    return redirect(url_for('dobavs_index'))


def _build_from_form(id, form):
    """ Инициализация экземпляра на основе формы """
    dobav = Dobav.get(id=id)

    if form:
        dobav.name = form.get('name', dobav.name)
        dobav.url = form.get('url', dobav.url)
        dobav.count = form.get('count', dobav.count)
        dobav.info = form.get('info', dobav.info)
        dobav.dobav_id = form.get('dobav_id', dobav.dobav_id)

    return dobav


def _filter(args):
    res = { 'statuses':  STATUSES}
    res['status'] = args.get('status', None)

    return res


def _filter_dobavs(filter):
    dobavs = Dobav.select()

    if filter.get("status"):
        dobavs = dobavs.where(Dobav.status == filter["status"])

    dobavs = dobavs.order_by(Dobav.count.asc())
    return dobavs
