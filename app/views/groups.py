from flask import render_template, redirect, request, url_for, flash
from flask_login import login_required, current_user

from app.flask import app

from app.models.group import Group


@app.route('/groups')
@login_required
def groups_index():
    groups = Group.select().where(Group.user_id == current_user.id)
    return render_template('groups/index.haml', groups=groups)


@app.route('/groups/new')
@login_required
def groups_new():
    group = _build_from_form(None, None)
    return render_template('groups/form.haml', group=group, url='/groups')


@app.route('/groups', methods=['POST'])
@login_required
def groups_create():
    group = _build_from_form(None, request.form)

    # TODO: Нужно это вынести в миксин валидации. Сделаю это как-нибудь позже
    if not _uniqueness_group(group):
        flash(u"Адрес группы уже есть в базе", 'danger')
        return render_template('groups/form.haml', group=group)
    if group.save():
        flash(u"Добавлена новая группа: {}".format(group.name), 'success')
        return redirect(url_for('groups_index'))
    else:
        return render_template('groups/form.haml', group=group)

@app.route('/groups/<int:id>/edit')
@login_required
def groups_edit(id):
    group = _build_from_form(id, None)
    return render_template('groups/form.haml', group=group, url='/groups/{}'.format(id))


@app.route('/groups/<int:id>', methods=['POST'])
@login_required
def groups_update(id):
    group = _build_from_form(id, request.form)

    # TODO: Нужно это вынести в миксин валидации. Сделаю это как-нибудь позже
    if not _uniqueness_group(group):
        flash(u"Адрес группы уже есть в базе", 'danger')
        return render_template('groups/form.haml', group=group)
    if group.save():
        flash(u"Группа успешно обновлена: {}".format(group.name), 'success')
        return redirect(url_for('groups_index'))
    else:
        return render_template('groups/form.haml', group=group)


@app.route('/groups/<int:id>/delete')
@login_required
def groups_delete(id):
    group = Group.get(id=id)
    group.delete_instance()
    flash(u"Группа успешно удалена: {}".format(group.name), 'success')
    return redirect(url_for('groups_index'))









def _build_from_form(id, form):
    """ Инициализация экземпляра на основе формы """
    if id:
        group = Group.get(id=id)
    else:
        group = Group(user_id=current_user.id)

    if form:
        group.name = form.get('name', group.name)
        group.url = form.get('url', group.url)
        group.count = form.get('count', group.count)
        group.info = form.get('info', group.info)
        group.group_id = form.get('group_id', group.group_id)

    return group


def _uniqueness_group(group):
    """ Проверка уникальности группы """

    try:
        if group.is_new():
            Group.select().where(Group.url == group.url).get()
        else:
            Group.select().where(Group.id != group.id, Group.url == group.url).get()

        return False
    except Group.DoesNotExist:
        return True
