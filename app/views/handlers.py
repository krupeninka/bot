from datetime import datetime

from flask import render_template, redirect, request, url_for
from flask_login import login_required, current_user

from app.flask import app

from app.models.handler import Handler, HANDLERS
from app.models.handler_task import HandlerTask
from app.models.handler_task_log import HandlerTaskLog
from app.models.mass_report import MassReport
from app.views._form_helper import FormHelper


@app.route('/handlers')
@login_required
def handlers_index():
    handlers = Handler.select()
    return render_template('handlers/index.haml', handlers=handlers)


@app.route('/handlers/<id>')
@login_required
def handlers_show(id):
    handler = Handler.get(Handler.id == id)
    tasks = handler.tasks.order_by(HandlerTask.created_at.desc())
    return render_template('handlers/show.haml', handler=handler, tasks=tasks, log_by_status=_log_by_status)


@app.route('/handlers/<id>/edit')
@login_required
def handlers_edit(id):
    handler = Handler.get(Handler.id == id)
    helper = FormHelper(handler, '/mass_reports')
    mass_reports = MassReport.select().where(MassReport.status == 'enabled')
    mass_reports = dict((m.id, m.name) for m in mass_reports )
    return render_template('handlers/edit.haml', handler=handler, helper=helper, mass_reports=mass_reports)


@app.route('/handlers/<id>/update', methods=['POST'])
@login_required
def handlers_update(id):
    errors = []
    handler = Handler.get(Handler.id == id)
    handler.next_step = request.form['next_step']
    handler.status = request.form['status']
    _update_options(handler, request.form)

    if handler.save():
        return redirect(url_for('handlers_edit', id = handler.id))

    return render_template('handlers/edit.haml', handler=handler)


@app.route('/handlers/<id>/run_now', methods=['POST', 'GET'])
@login_required
def handlers_run_now(id):
    errors = []
    handler = Handler.get(Handler.id == id)
    handler.next_run = datetime.now()
    handler.save()
    return redirect(url_for('handlers_edit', id = handler.id))


def _log_by_status(task, status):
    """ Полученение логов по статусу """
    return task.logs.where(HandlerTaskLog.status == status)


def _update_options(handler, form):
    """ Обновление и проверка параметра options. Ошибки формируются как в валидаторе ValidateMixin """
    def validate_key(value):
        app.logger.info(', '.join([key, value]))

    for key in HANDLERS[handler.name].get('options').keys():
            value = form.get(key) or ''
            handler.options[key] = value
