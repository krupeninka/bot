import os
import yaml

from app import root

from peewee import PostgresqlDatabase

path = os.path.join(root, 'config', 'database.yml')
config = yaml.load(open(path, 'r'))

# Подключение к  базе PostgreSQL
db = PostgresqlDatabase(
        config.get('database'),
        user = config.get('username'),
        password = config.get('password'),
        host = config.get('host'),
        port = config.get('port', 5432),
        autorollback = True)
