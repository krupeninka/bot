import requests
import random
import time

from app.handlers._base_handler import BaseHandler

class MassReportFriendsHandler(BaseHandler):
    """ Репост со страницы группы """

    stat_key = 'mass_report_friends'


    def _execute(self):
        """ Алгоритм который нужно исполнить """
        anegdot = requests.get('http://umorili.herokuapp.com/api/get?site=bash.im&name=bash&num=100').json()[random.randint(0,99)]['elementPureHtml']
        anegdot = anegdot.replace('<br />', '\n')
        anegdot = anegdot.replace('&quot;', '"')

        # print("\tОтправка сообщений пользователям")
        # print(anegdot)

        self.driver.get('https://vk.com/friends?section=online')
        self.driver.scroll()

        urls = [element.get_attribute('href') for element in self.driver.finds('.friends_field.friends_field_title a')]

        url = urls[random.randint(0,len(urls) -1)]

        self.driver.get(url)
        self.driver.click('button.flat_button.profile_btn_cut_left')
        time.sleep(2)

        message = "Вижу все еще не спишь.\nНашел прикольный анекдот.\n\n" + anegdot
        self.driver.input('#mail_box_editable', message)
        self.driver.click('#mail_box_send')
