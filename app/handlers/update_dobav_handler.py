import re
import datetime
import time

from app.handlers._base_handler import BaseHandler
from app.models.dobav import Dobav

class UpdateDobavHandler(BaseHandler):
    """ Обновление информации о группах "Добавь" """

    stat_key = 'update_dobav'


    def _execute(self):
        """ Алгоритм который нужно исполнить """

        # self._find_new_group()
        self._update_group()



    def _find_new_group(self, scroll=1):
        """ Поиск новых групп """
        words = ['Добавь', 'Пиар']

        url = 'https://vk.com/search?c[per_page]=40&c[q]={}&c[section]=communities'

        for word in words:
            self.driver.get(url.format(word))
            self.driver.scroll(down=True)

            links = [element for element in self.driver.finds('.groups_row .info .labeled.title a')]
            for link in links:
                dobav, is_new = Dobav.get_or_create(url=link.get_attribute('href'),
                        defaults={'status': 'new', 'name': link.text, 'count': 0, 'updated_at': datetime.datetime.now() })

                if is_new:
                    self.stat_value += 1

                if self.stat_value >= 5:
                    return



    def _update_group(self):
        """Обновление информации о группе"""
        groups = Dobav.select().order_by(Dobav.updated_at.asc()).limit(5)
        for group in groups:
            self.driver.get(group.url)
            group.count = 0
            # print(group.url, group.name)

            # print(self.driver.find('.placeholder .ph_input .ph_content').text)
            # if not self.driver.finds('.placeholder .ph_input .ph_content'):
            #     group.status = 'close_wall'
            # el

            if self.driver.in_source('https://vk.com/images/error404.png'):
                group.status = 'error'
            elif not self.driver.is_exist('.placeholder .ph_input .ph_content'):
                group.status = 'close_wall'
            elif self.driver.find('.placeholder .ph_input .ph_content').text == 'Предложите новость':
                group.status = 'close_wall'
            elif self.driver.find('.placeholder .ph_input .ph_content').text == 'Добавить запись…':
                group.status = 'open_wall'
            else:
                raise


            group.count = self._get_user_count() or group.count # КОСТЫЛЬ: Не всегда можем найти количество, например на удаленных группах
            group.updated_at = datetime.datetime.now()
            group.save()
            self.stat_value += 1
            # time.sleep(60)


    def _get_user_count(self):
        """ Получаем количество подписчиков в группе """
        blocks = self.driver.finds('.module_header')
        for block in blocks:
            # print(block.text)
            if block.find_element_by_css_selector('.header_label.fl_l').text in ('Подписчики', 'Участники'):
                # print(block.find_element_by_css_selector('.header_count.fl_l').text)
                return block.find_element_by_css_selector('.header_count.fl_l').text.replace(' ', '')

