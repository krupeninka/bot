import re
import random

from app.handlers._base_handler import BaseHandler

from app.models.mass_report import MassReport
from app.models.mass_report_info import MassReportInfo
from app.models.cache import Cache

class Page():
    """ Класс страницы пользователя """


    def __init__(self, driver, url, mass_report):
        """ Ициализация обекта и загрузка страницы """
        self.mass_report = mass_report
        self.driver = driver
        self.driver.get(url)

        self._user_id = None


    @property
    def user_id(self):
        """ Получаем id пользователя """
        if not self._user_id:
            ava_url = self.driver.find('a#profile_photo_link').get_attribute('href')
            self._user_id = re.findall('\d+', ava_url)[0]



        return self._user_id


    def is_valid(self):
        """ Проверка страницы, нужно ли делать отправку данному пользователю """


        # Если нет кнопки отправить сообщение, не отправляем сообщение
        if not self.driver.finds('#profile_message_send.profile_action_btn.profile_msg_split'):
            return False

        # Если у пользователя закрытый профиль, скорей всего ему лучше не отправлять
        if not self.driver.is_exist('a#profile_photo_link'):
            return False


        # Проверяем по базе
        exists = MassReportInfo.select()\
                .where(MassReportInfo.mass_report_id == self.mass_report.id,
                        MassReportInfo.user_id == self.user_id)\
                .exists()
        if exists:
            return False

        return True



class MassReportHandler(BaseHandler):
    """ Массовая рассылка сообщений абонентам из списка """

    stat_key = 'mass_report'


    def _execute(self):
        """ Алгоритм который нужно исполнить """
        mass_report = MassReport.get(MassReport.id == self.handler.options['mass_report_id'])
        # Получаем список адресов страниц пользователей
        urls = self._get_users_url(mass_report.url)
        # Проверяем пользователя на ранее отправленные сообщения,
        page = self._get_user_page(urls, mass_report)
        # Отправляем сообщение
        self._send_message(mass_report, page)


    def _get_users_url(self, url):
        """ Получение списка пользователей из поиска """

        self.driver.get(url)
        self.driver.scroll(count=20, sleep=5)

        self.urls = [element.get_attribute('href') for element in self.driver.finds('.people_row .info .labeled.name a')]

        self.all_count = self.driver.find('.page_block_header_count').text
        self.url_count = len(self.urls)

        return self.urls


    def _get_user_page(self, urls, mass_report):
        """ Получаем пользователя которому не отправлялось сообщение """

        for url in urls:
            # Проверяем страницу в кэше
            cache_key = "massreport-{}-{}".format(mass_report.id, url)
            if Cache.select().where(Cache.key == cache_key).exists():
                continue


            page = Page(self.driver, url, mass_report)

            if not page.is_valid():
                Cache.create(key=cache_key)
                continue

            return page

        # Если пробежали весь список, возвращаем ошибку
        raise Exception("Список страниц кончился {}/{}".format(self.url_count,self.all_count))


    def _send_message(self, mass_report, page):
        """ Отправка сообщения пользователю """

        message = self._get_message(mass_report, page)
        # self.driver.get('https://vk.com/im?sel=247702872')
        self.driver.get('https://vk.com/im?sel=' + page.user_id)
        self.driver.input('.im_editable.im-chat-input--text._im_text', message)
        self.driver.click('.im-send-btn.im-chat-input--send._im_send.im-send-btn_send')


        MassReportInfo.create(user_id = page.user_id, mass_report_id = mass_report.id, bot_id = self.handler.bot.id)
        self.stat_value += 1


    def _get_message(self, mass_report, page):
        """ Выбираем рандомное сообщение и подставляем данные страницы """
        messages = mass_report.messages.split('\n---')
        count = len(messages)
        index = random.randint(0, count - 1)
        message = messages[index]
        message = message.format(page=page)

        return message
