import random

from app.handlers._base_handler import BaseHandler
from app.models.dobav import Dobav

class PostingInGroupHandler(BaseHandler):
    """ Постинг сообщения на стену группы """

    stat_key = 'posting_in_group'
    stat_value = 0

    count_lost = 0
    messages = []
    groups_lost = []

    def _execute(self):
        """ Алгоритм который нужно исполнить """
        dobavs = Dobav.select().where(Dobav.status == 'open_wall', Dobav.count < 20000,  Dobav.count > 1000)

        self.groups_lost = [dobav.url for dobav in dobavs]
        self.count_lost = int(self.handler.options['count'])
        self.messages = self.handler.options['messages'].split('---')

        while self.count_lost > 0 and len(self.groups_lost) > 0:
            url = self._get_group_url()
            message = self._get_random_message()
            self.posting(url, message)

    def _get_random_message(self):
        """ Получаем случайное сообщение """

        index = random.randint(0, len(self.messages) - 1)
        return self.messages[index]


    def _get_group_url(self):
        """ Получаем адрес группы для перехода, и удаляем ее из списка """

        index = random.randint(0, len(self.groups_lost) - 1)

        url = self.groups_lost[index]
        del self.groups_lost[index]
        return url


    def posting(self, url, message):
        """ Постинг сообщения на стену группы """
        # Если нет поля для ввода, пропускаем
        # if not self.driver.find('#send_post').text == 'Добавить запись…':
        #     print("Нельзя добавить запись", url)
        #     return


        self.driver.get(url)
        posts = self.driver.finds('.post_author a')
        for post in posts[:5]:
            if post.text == self.handler.bot.name:
                return

        self.driver.input('#post_field', message)
        if self.driver.find('#send_post').text == 'Предложить новость':
                print("Публичная страница", url)
                return

        self.driver.click('#send_post', sleep=2)

        if self.driver.finds('.box_layout .box_title_wrap .box_title'):
            print("Captcha:", url)
            return

        # По непонятным причинам не всегда постится запись
        if not self.driver.find('#post_field').text == '':
            self.driver.click('#send_post', sleep=2)

        if not self.driver.find('#post_field').text == '':
            print(self.driver.find('#post_field').text)



        self.count_lost -=1
        self.stat_value += 1