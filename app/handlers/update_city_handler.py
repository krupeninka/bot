# TODO:
# * Добавить проверку вступил ли человек

import re
import datetime
import time

from app.handlers._base_handler import BaseHandler
from app.models.city import City

class UpdateCityHandler(BaseHandler):
    """ Обновление информации о городах """

    stat_key = 'update_city'


    def _execute(self):
        """ Алгоритм который нужно исполнить """

        # self._add_new()
        self._update_count()
        self._update_rival()


    def _add_new(self):
        """ Добавление новых городов """
        try:
            max_id = City.select().order_by(City.id.desc()).get().id
        except City.DoesNotExist:
            max_id = 0

        while True:
            max_id += 1
            self.driver.get("https://vk.com/search?c[city]={}&c[country]=1&c[per_page]=40&c[photo]=1&c[section]=people".format(max_id))
            name = self.driver.finds('#container2 table tbody tr td input')[0].get_attribute('value')

            if name == 'DELETED':
                continue

            City.get_or_create(id=max_id, defaults = { "name": name, "count": 0, "updated_at": datetime.datetime.now(), "options": {} })
            print(max_id, name)

            self.stat_value += 1

            # time.sleep(5)

    def _update_count(self):
        """ Обновление количества людей """

        for city in City.select().order_by(City.updated_at.asc()).limit(int(self.handler.options['count'])):
            # print(city.id, city.name)
            self.driver.get("https://vk.com/search?c[city]={}&c[country]=1&c[per_page]=40&c[photo]=1&c[section]=people".format(city.id))
            text = self.driver.find('span.page_block_header_count').text.replace(' ', '')
            if not text == '':
                city.count = int(text)
            city.updated_at = datetime.datetime.now()
            city.save()

            self.stat_value += 1
            # print(city.id, city.name, city.count)



    def _update_rival(self):
        """ Обновление данных о конкуренте в городе """
        # print("Обновление данных о конкурентах")
