import time

from selenium.common.exceptions import StaleElementReferenceException

from app.handlers._base_handler import BaseHandler


class AddFriendHandler(BaseHandler):
    """ Подтверждение заявок в друзья """

    stat_key = 'add_friend'


    def _execute(self):
        """ Алгоритм который нужно исполнить """

        self.driver.get('https://vk.com/friends?section=requests')
        # self.driver.scroll()
        selector = 'div.friends_user_row.friends_user_common.friends_user_request.clear_fix button.flat_button.button_small'
        for button in self.driver.finds(selector):
            try:
                button.click()
            except StaleElementReferenceException:
                # Падает при нажатии на кнопку. На работу это не влияет, поэтому просто пропускаем
                pass
            self.stat_value += 1
            time.sleep(1)

        if self.stat_value:
            self.log('success', "Добавлено {} человек".format(self.stat_value))
