import time

from app.handlers._base_handler import BaseHandler

class UnfolowHandler(BaseHandler):
    """ Выход из подписчиков """
    stat_key = 'unfolow'

    def _execute(self):
        """ Алгоритм который нужно исполнить """

        self.driver.get('https://vk.com/friends?section=out_requests')
        # self.driver.scroll()

        for button in self.driver.finds('button.flat_button.button_small.fl_r'):
            button.click()
            self.stat_value += 1
            time.sleep(1)

        if self.stat_value:
            self.log('success', "Отписано от {} человек".format(self.stat_value))

