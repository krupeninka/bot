from app.handlers._base_handler import BaseHandler

from app.models.mass_report import MassReport

class RepostHandler(BaseHandler):
    """ Репост со страницы группы """

    stat_key = 'mass_report'

    def _execute(self):
        """ Алгоритм который нужно исполнить """
        mass_report = MassReport.get(self.handler.options['mass_report_id'])
        print(mass_report.id)
