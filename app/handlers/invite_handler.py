# TODO:
# * Добавить проверку вступил ли человек

import re

from app.models.invite_info import InviteInfo
from app.handlers._base_handler import BaseHandler

class InviteHandler(BaseHandler):
    """ Выход из подписчиков """

    stat_key = 'invite'


    def _execute(self):
        """ Алгоритм который нужно исполнить """

        self.driver.get(self.handler.options['search'])

        for button in self.driver.finds('div.friends_controls .flat_button.button_small.button_wide'):
            # Из кнопки получаем группу и пользователя
            group_id, user_id = re.findall(r'(\d+), (\d+)', button.get_attribute('onclick'))[0]

            # Если проверка пользователя не пройдена, переходим к следующему
            if not self._check_in_db(group_id, user_id):
                continue

            # Нажимем кнопку
            button.click()

            # Сохраняем в базу
            InviteInfo.create(user_id = user_id, group_id = group_id, bot = self.handler.bot)
            self.stat_value += 1

            break


    def _check_in_db(self, group_id, user_id):
        """
            Проверяем по базе были ли этому пользователю приглашения.
            Если не было, можно отправлять, возвращаем True
            Если были из этой группы, то пользователь пропускается, возвращаем False
            Если были не из этой группы, смотрим чего больше,
                если игнорировал чаще,  то пользователь пропускается, возвращаем False
                если вступал чаще, можно отправлять, возвращаем False
        """

        enter_count = 0
        ignore_count = 0

        for info in InviteInfo.select().where(InviteInfo.user_id == user_id):
            if info.group_id == group_id:
                return False
            elif info.status == 'user_еnter':
                enter_count += 1
            else:
                ignore_count += 1

        if ignore_count > enter_count :
            return False
        else:
            return True
