import traceback

from datetime import datetime, timedelta, date, time

from app.models.bot_stat import BotStat
from app.models.handler_task import HandlerTask
from app.models.handler_task_log import HandlerTaskLog

class BaseHandler(object):
    """
        В проекте присутствует два понятия Handler, в контексте приложения они разделяются на:
        - Обработка - это задача которая ставится на аккаунт соц. сети.
        - Обработчик - это класс описания способа выполнения задачи

        Базовый класс экранирует работу с объктом класса HundlerTask
    """

    stat_key = 'base'
    stat_value = 0


    def __init__(self, handler, driver):
        """ Инициализация объекта """

        self.handler = handler
        self.driver = driver
        self.task = HandlerTask(handler=self.handler)

        # Дополняем аттрибуты из словаря handler.options
        for key, value in handler.options.items():
            setattr(self,key, value)


    def execute(self):
        """ Исполнение обработчика """
        self.handler.last_run = datetime.now()
        self.task.save()
        try:
            self._execute()
        except:
            HandlerTaskLog.create(status='critical', handler_task=self.task,
                        message = "Критическая ошибка", traceback=traceback.format_exc())
            print(traceback.format_exc())
        # Сохраняем статистику
        stat, _ = BotStat.get_or_create(date = date.today(), bot = self.handler.bot, key = self.stat_key,
                defaults = { 'value': 0 } )
        stat.value += self.stat_value
        stat.save()
        # Сохраняем данные по выполнению задачи
        self.task.finished_at = datetime.now()
        self.task.save()

        # Устанавливаем следующий запуск скрипта. Если выполнили максимальное количество за день,
        # ставим следующий день 9:00
        if self.handler.value_option('max_count') != "Не найдено" and \
                stat.value >= int(self.handler.value_option('max_count')):
            self.handler.next_run = datetime.combine(date.today(), time()) + timedelta(hours=30)
        else:
            self.handler.next_run = datetime.now() + timedelta(minutes=self.handler.next_step)


        # Сохраняем данные по обработке
        self.handler.save()


    def log(self, status, message):
        """ Сохранение логов в базу """
        HandlerTaskLog.create(status=status, handler_task=self.task,
                        message = message)


    def _execute(self):
        """ Алгоритм который нужно исполнить """
        pass
