import time
import random

from app.handlers._base_handler import BaseHandler
from app.models.cache import Cache


class LikeHandler(BaseHandler):
    """ Постановка лайков пользователям из фильтра """

    stat_key = 'like'
    liked_count = 0
    lost_count = None
    pages = []


    def _execute(self):
        """ Алгоритм который нужно исполнить """

        self.lost_count = int(self.count)
        self._get_pages()

        while self.lost_count > 0:
            result = self._like_pages()


    def _get_pages(self):
        """ Получение списка пользователей из поиска """

        self.driver.get(self.handler.options['search'])
        self.driver.scroll(count=int(self.count_like) * 3, sleep=3)

        self.pages = [element.get_attribute('href') for element in self.driver.finds('.people_row .info .labeled.name a')]

        return self.pages


    def _get_page(self):
        """ Получаем страницу для перехода, и удаляем ее из списка """

        index = random.randint(0, len(self.pages) - 1)

        url = self.pages[index]
        del self.pages[index]

        # Проверяем страницу в кэше
        cache_key = "like-{}-{}".format(self.handler.bot.id, url)
        if Cache.select().where(Cache.key == cache_key).exists():
            return self._get_page()

        return url


    def _like_pages(self):
        """ Рекурсивно переходим по страницам, и проставляем лайки"""

        liked = False
        count_like = int(self.count_like)
        url = self._get_page()
        cache_key = "like-{}-{}".format(self.handler.bot.id, url)

        self.driver.get(url, sleep=2)

        # Не на всех страницах есть аватарка, просто пропускаем, и продалжаем обработку
        if self.driver.is_exist('div.wide_column_left'):
            Cache.create(key=cache_key)
            return self._like_pages()

        # Открываем аву
        self.driver.click('img.page_avatar_img', sleep=2, wait=1)

        # Если лайков больше сотни игнорим
        like_on_ava = int(self.driver.find('div.like_button_count').text.replace(' ', '') or 0)
        if like_on_ava > 100:
            Cache.create(key=cache_key)
            return self._like_pages()

        # Если лайка нет, ставим, записываем в cтатистику
        # print("div.like_btns a.active:", self.driver.is_exist('div.like_btns a.active'))
        if not self.driver.is_exist('div.ui_scroll_content.clear_fix div.like_wrap div.like_cont div.like_btns a.active'):
            self.driver.click('div.like_button_icon', wait=1, sleep=2)
            self.stat_value += 1

            self.driver.click('div.pv_close_btn')
            liked = True
            Cache.create(key=cache_key)
        else:
            Cache.create(key=cache_key)

        # Лайкаем стену, если количество лайков больше 1
        if count_like > 1:
            self.driver.get(url, sleep=2)

            posts = self.driver.finds('.post_full_like_wrap.clear_fix')[:count_like-1]
            for post in posts:

                # Если нет лайка, ставим, записываем статистику
                if not post.find_elements_by_css_selector('.post_full_like a.post_like._like_wrap.my_like'):
                    post.find_element_by_css_selector('.post_full_like a.post_like._like_wrap').click()
                    # print('\t\t like post')
                    self.stat_value += 1
                    liked = True

        # Если был ходь один лак уменьшаем количество на 1
        if liked:
            self.lost_count -= 1
