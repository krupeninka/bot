import re
import datetime
import time

from app.handlers._base_handler import BaseHandler
from app.models.city import City
from app.models.darom import Darom

class UpdateDaromHandler(BaseHandler):
    """ Обновление информации о городах """

    stat_key = 'update_darom'


    def _execute(self):
        """ Алгоритм который нужно исполнить """

        self._add_new()
        self._update_count()
        # self._update_rival()


    def _add_new(self):
        """ Добавление новых городов """
        cities = City.select().where(City.count > 100000, City.count < 1000000).order_by(City.count.desc())
        for city in cities:

            Darom.get_or_create(city=city, defaults={'status': 'new', 'max_count': 0, 'group_count': 0, 'count': 0, 'persent': 0, "updated_at": datetime.datetime.now(), "options": {}})


    def _update_count(self):
        """ Обновление количества людей """

        for darom in Darom.select().where(Darom.status << ['new', 'active']).order_by(Darom.updated_at.asc()).limit(int(self.handler.options['count'])):

            self.driver.get("https://vk.com/groups?act=catalog&c[like_hints]=1&c[per_page]=40&c[q]=Даром%20{}&c[section]=communities".format(darom.city.name))

            # Количество групп
            text = self.driver.find('span.page_block_header_count').text.replace(' ', '')
            if text:
                count = int(text)
            else:
                count = 0

            darom.group_count = count

            # Максимальное количество
            if self.driver.is_exist('div#results div.groups_row.search_row.clear_fix'):
                text = self.driver.finds('div#results div.info')[0].find_elements_by_css_selector('div.labeled')[-1].text
            else:
                text = '0'

            text = re.findall('\d+', text)
            text = ''.join(text)
            count = int(text)

            darom.max_count = count



            darom.persent = int(darom.max_count / darom.city.count * 100)

            darom.status = 'active'
            darom.updated_at = datetime.datetime.now()
            darom.save()

            # text = self.driver.find('span.page_block_header_count').text.replace(' ', '')
            # if not text == '':
            #     city.count = int(text)
            # city.updated_at = datetime.datetime.now()
            # city.save()

            # # print(city.id, city.name, city.count)
            time.sleep(1)
            self.stat_value += 1



    def _update_rival(self):
        """ Обновление данных о конкуренте в городе """
        # print("Обновление данных о конкурентах")
