import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options




DEFAULT_SCREEN_PATH = './screen.png' # cохраняем файл в том месте где запускали скрипт


class DriverException(Exception):
    """Ошибка при работе с драйвером phantomjs"""


class BaseDriver(webdriver.Chrome):
    """Класс обертка, для упрощения работы с драйвером phantomjs"""

    default_sleep = 0.5

    def __init__(self, default_sleep = None):
        """ Обертка для инициализации драйвера """
        opts = Options()
        # opts.binary_location = '/usr/bin/chromium-browser'
        super(BaseDriver, self).__init__('/var/apps/piar/chromedriver', chrome_options=opts)

        if default_sleep:
            self.default_sleep = default_sleep


    @property
    def source(self):
        """ Алиас для метода page_source """
        return self.page_source


    def get(self, url, sleep=None):
        """ Переопределение мето get, для установки времени ожидания """
        super(BaseDriver, self).get(url)
        time.sleep(sleep or self.default_sleep)


    def find(self, selector):
        """ Алиас для метода find_element_by_css_selector """
        return self.find_element_by_css_selector(selector)


    def finds(self, selector):
        """ Алиас для метода find_elements_by_css_selector """
        return self.find_elements_by_css_selector(selector)


    def is_exist(self, selector):
        """ Проверка наличая элемента """
        if self.finds(selector):
            return True
        return False


    def screen(self, path = DEFAULT_SCREEN_PATH):
        """Алиас для метода save_screenshot"""
        if not path:
            path = DEFAULT_SCREEN_PATH

        self.save_screenshot(path)


    def in_source(self, str):
        """ Проверка вхождения стрики в содержимое странице """
        if str in self.source:
            return True
        else:
            return False


    def scroll(self, count=0, height=None, down=False, sleep=2):
        """ Скролинг страницы вниз """
        height = height or self.execute_script("return document.documentElement.scrollHeight")
        self.execute_script("window.scrollTo(0, {});".format(height))
        time.sleep(sleep)

        if down or count > 0:
            # print(down, count, self.execute_script("return document.documentElement.scrollHeight"), height)
            if self.execute_script("return document.documentElement.scrollHeight") > height:
                self.scroll(down=down, count=count - 1)


    def input(self, selector, keys):
        """ Заполнение поля по селектору """
        return self.find(selector).send_keys(keys)


    def click(self, selector, sleep=0, wait=0):
        """ Клик по элементу с выбором по селектору """
        try:
            self.find(selector).click()
            time.sleep(sleep)
            return True
        except NoSuchElementException as ex:
            if wait:
                time.sleep(1)
                return self.click(selector, sleep, wait - 1)

            raise ex

        return False