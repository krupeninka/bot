import time

from app.drivers.base_driver import BaseDriver


class VktargetDriver(BaseDriver):
    """ Драйвер для работы с youtube.com """


    def login(self, login, password):
        """ Авторихация в контакте """

        def check_login(wait):
            """ Функция проверки авторизиции и ожидание действий пользователя, если требуется """

            time.sleep(wait)

            if self.in_source(login):
                return True
            else:
                print('Ожидание авторизации: {} сек.'.format(wait + 1))
                return check_login(wait + 1)
                # raise Exeption("Ошибка авторизации пользователя")



        self.get('https://vktarget.ru')
        self.finds('.vkt-login__form input')[0].send_keys(login)
        self.finds('.vkt-login__form input')[1].send_keys(password)
        self.find('.vkt-login__form .vkt-menu__item').click()

        return check_login(0)


# 612623