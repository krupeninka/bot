import time

from app.drivers.base_driver import BaseDriver


class VkDriver(BaseDriver):
    """ Драйвер для работы с youtube.com """

    def get(self, url, sleep=None):
        """
            Переопределение мето get, для установки времени ожидания.
            Так же в ВК есть
        """
        super(VkDriver, self).get(url)
        time.sleep(sleep or self.default_sleep)

        if self.is_exist('div.box_layout'):
            self.click('div.box_x_button')


    def login(self, login, password):
        """ Авторихация в контакте """

        def check_login(wait):
            """ Функция проверки авторизиции и ожидание действий пользователя если требуется """

            time.sleep(wait)

            if self.in_source('Моя Страница'):
                return True
            elif self.in_source('Впервые ВКонтакте?'):
                print('Авторизация, ожидание: {} сек.'.format(wait + 1))
                return check_login(wait + 1)
            elif self.in_source('Проверка безопасности'):
                print('Проверка безопасности, ожидание: {} сек.'.format(wait + 1))
                return check_login(wait + 1)
            else:
                raise Exception("Ошибка авторизации пользователя")



        self.get('https://vk.com/')
        self.input('#index_email', login)
        self.input('#index_pass', password)
        self.click('#index_login_button')

        return check_login(0)